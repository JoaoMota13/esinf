/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.time.LocalDate;

/**
 *
 * @author DEI-ISEP
 */
public class Invoice implements Comparable<Invoice> {
    private String reference;
    private LocalDate date;
    
    public Invoice(String reference, String date) {
        this.setReference(reference);
        if (date!=null) {
            String s[] = date.split("/");
            this.setDate(LocalDate.of(Integer.parseInt(s[0]), Integer.parseInt(s[1]), Integer.parseInt(s[2])));
        }
    }
    
    public Invoice(String reference) {
        this(reference, "1900/01/01");
    }    

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

   @Override
    public boolean equals(Object obj) {
       return obj != null && ((Invoice)obj).getReference().equalsIgnoreCase((this.getReference()));
    }
    
    @Override
    public int hashCode() {
        return this.getReference().hashCode();
    }

    @Override
    public int compareTo(Invoice o) {
        /*int invoiceRefNumber = parseReferenceAsInt(o.getReference());
        int currentRefNumber = parseReferenceAsInt(this.getReference());

        if (o != null && invoiceRefNumber > 0 && currentRefNumber > 0) {
            if (currentRefNumber > invoiceRefNumber) {
                return 1;
            } else if(currentRefNumber == invoiceRefNumber) {
                return 0;
            } else {
                return -1;
            }
        }

        return 1;*/
        return this.getReference().compareTo(o.getReference());
    }

    private int parseReferenceAsInt(String reference) {
        if(reference != null && reference.length() > 0) {
            return Integer.parseInt(reference.replace("Invoice", "").replace("INV", ""));
        }

        return 0;
    }

}