import java.util.*;

public class LinkedLists {

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<Integer>();
        list.add(2);
        list.add(9);
        list.add(7);
        list.add(5);
        list.add(10);
        list.add(15);
        list.add(6);
        list.add(12);
        list.add(30);

        ArrayList<Integer> centers = new ArrayList<Integer>(Arrays.asList(3, 6, 10));

        Map<Integer, LinkedList<Integer>> result = KSubLists(list, centers);

        for (Map.Entry<Integer, LinkedList<Integer>> entry : result.entrySet()) {
            String line = String.format("{%d", entry.getKey());

            for (Integer num : entry.getValue()) {
                line = String.format("%s, %d", line, num);
            }

            System.out.println(String.format("%s}", line));
        }

    }

    public static Map<Integer, LinkedList<Integer>> KSubLists(LinkedList<Integer> list, ArrayList<Integer> centers) {
        Map<Integer, LinkedList<Integer>> mapa = new HashMap<Integer, LinkedList<Integer>>(centers.size());

        LinkedList<LinkedList<Integer>> subLists = new LinkedList<LinkedList<Integer>>();

        for (int i = 0; i < centers.size(); i++) {
            subLists.add(new LinkedList<Integer>());
        }

        for(Integer num : list) {
            Integer min = Integer.MAX_VALUE;
            int index = 0;
            for (int i = 0; i < centers.size(); i++) {
                if (Math.abs(num - centers.get(i)) < min) {
                    min = Math.abs(num - centers.get(i));
                    index = i;
                }
            }
            subLists.get(index).add(num);
        }

        for (int i = 0; i < centers.size(); i++) {
            mapa.put(centers.get(i), subLists.get(i));
        }

        return mapa;
    }
}
