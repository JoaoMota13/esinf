/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;


/**
 *
 * @author DEI-ISEP
 * @param <E> Generic list element type
 */
public class DoublyLinkedList<E> implements Iterable<E>, Cloneable {

// instance variables of the DoublyLinkedList
    private final Node<E> header;     // header sentinel
    private final Node<E> trailer;    // trailer sentinel
    private int size = 0;       // number of elements in the list
    private int modCount = 0;   // number of modifications to the list (adds or removes)

    /**
     * Creates both elements which act as sentinels
     */
    public DoublyLinkedList() {

        header = new Node<>(null, null, null);      // create header
        trailer = new Node<>(null, header, null);   // trailer is preceded by header
        header.setNext(trailer);                    // header is followed by trailer
    }

    /**
     * Returns the number of elements in the linked list
     *
     * @return the number of elements in the linked list
     */
    public int size() {
        return this.size;
    }

    /**
     * Checks if the list is empty
     *
     * @return true if the list is empty, and false otherwise
     */
    public boolean isEmpty() {
        return size == 0;
    }

    /**
     * Returns (but does not remove) the first element in the list
     *
     * @return the first element of the list
     */
    public E first() {
        if (header.getNext() != null) {
            return header.getNext().getElement();
        }

        return new Node<E>(null, null, null).getElement();
    }

    /**
     * Returns (but does not remove) the last element in the list
     *
     * @return the last element of the list
     */
    public E last() {
        if (trailer.getPrev() != null) {
            return trailer.getPrev().element;
        }

        return new Node<>(null, header, trailer).element;
    }

// public update methods
    /**
     * Adds an element e to the front of the list
     *
     * @param e element to be added to the front of the list
     */
    public void addFirst(E e) {
        // place just after the header
        Node<E> nextElement = header.getNext();
        Node<E> newNode = new Node<>(e, header, nextElement);

        if(this.isEmpty()) {
            trailer.setPrev(newNode);
        }
        else {
            nextElement.setPrev(newNode);
        }

        header.setNext(newNode);

        size++;
        modCount++;
    }

    /**
     * Adds an element e to the end of the list
     *
     * @param e element to be added to the end of the list
     */
    public void addLast(E e) {
        // place just before the trailer
        Node<E> prev = trailer.getPrev();
        Node<E> newElement = new Node<>(e, prev, trailer);

        if(this.isEmpty()) {
            header.setNext(newElement);
        }
        else {
            prev.setNext(newElement);
        }

        trailer.setPrev(newElement);

        size++;
        modCount++;
    }

    /**
     * Removes and returns the first element of the list
     *
     * @return the first element of the list
     */
    public E removeFirst() {
        if(!this.isEmpty()) {
            Node<E> nodeToRemove = header.getNext();

            if(nodeToRemove.equals(trailer)) {
                header.setNext(trailer);
                trailer.setPrev(header);
            }
            else {
                Node<E> replacement = nodeToRemove.getNext();

                header.setNext(replacement);
                replacement.setPrev(header);
                size--;
                modCount++;

                return nodeToRemove.element;
            }
        }

        return null;
    }

    /**
     * Removes and returns the last element of the list
     *
     * @return the last element of the list
     */
    public E removeLast() {
        if(!this.isEmpty()) {
            Node<E> nodeToRemove = trailer.getPrev();
            Node<E> replacement = nodeToRemove.getPrev();

            if(nodeToRemove.equals(header)) {
                trailer.setPrev(header);
                header.setNext(trailer);
            }
            else {
                trailer.setPrev(replacement);

                replacement.setNext(trailer);
                size--;
                modCount++;

                return nodeToRemove.element;
            }
        }

        return null;
    }
    
// private update methods

    /**
     * Adds an element e to the linked list between the two given nodes.
     */
    private void addBetween(E e, Node<E> predecessor, Node<E> successor) {
        Node<E> newElement = new Node<>(e, predecessor, successor);
        predecessor.setNext(newElement);
        successor.setPrev(newElement);

        size++;
        modCount++;
    }

    /**
     * Removes a given node from the list and returns its content.
     */
    private E remove(Node<E> node) {
        Node<E> previous = node.getPrev();
        Node<E> next = node.getNext();

        if(previous != null) {
            previous.setNext(next);
        }
        next.setPrev(previous);
        size--;
        modCount++;

        return node.getElement();
    }
 
// Overriden methods        
    @Override
    public boolean equals(Object obj) {
        if(obj != null && obj.getClass() == this.getClass()) {
            DoublyLinkedList<E> objectToCompare = (DoublyLinkedList<E>)obj;

            if(this.size() == objectToCompare.size()) {
                ListIterator<E> thisIt = this.listIterator();
                ListIterator<E> comparisonIt = objectToCompare.listIterator();

                boolean allMatch = true;
                for (int i = 0; i < this.size(); i++) {
                    if(thisIt.next() != comparisonIt.next()) {
                        allMatch = false;
                        i = this.size(); // exit for right away
                    }
                }

                return allMatch;
            }
        }

        return false;
    }
    
    @Override
    public Object clone() throws CloneNotSupportedException {
        super.clone();
        ListIterator<E> iterator = this.listIterator();

        DoublyLinkedList<E> newList = new DoublyLinkedList<>();
        ListIterator<E> newListIt = newList.listIterator();
        for (int i = 0; i < this.size(); i++) {
            newListIt.add(iterator.next());
        }

        return newList;
    }
    
//---------------- nested DoublyLinkedListIterator class ----------------        
    private class DoublyLinkedListIterator implements ListIterator<E> {

        private DoublyLinkedList.Node<E> nextNode,prevNode,lastReturnedNode; // node that will be returned using next and prev respectively
        private int nextIndex;  // Index of the next element
        private int expectedModCount;  // Expected number of modifications = modCount;

        public DoublyLinkedListIterator() {
            this.prevNode = header;
            this.nextNode = header.getNext();
            lastReturnedNode = null;
            nextIndex = 0;
            expectedModCount = modCount;
        }

        final void  checkForComodification() {  // invalidate iterator on list modification outside the iterator
            if (modCount != expectedModCount)
                throw new ConcurrentModificationException();
        }        
        
        @Override
        public boolean hasNext() {
            return nextNode != null && nextNode.getNext() != null;
        }

        @Override
        public E next() throws NoSuchElementException {
            checkForComodification();
            
            if(hasNext()) {
                Node<E> nodeToReturn = nextNode;
                prevNode = nextNode;
                nextNode = nextNode.getNext();

                nextIndex++;
                lastReturnedNode = nodeToReturn;

                return lastReturnedNode.getElement();

            }

            return null;
        }

        @Override
        public boolean hasPrevious() {            
            return nextIndex > 0;
        }

        @Override
        public E previous() throws NoSuchElementException{
            checkForComodification();

            if(hasPrevious()) {
                Node<E> nodeToReturn = nextNode.getPrev();
                nextNode = nextNode.getPrev();
                if(nextNode != null) {
                    prevNode = nextNode.getPrev();
                }

                nextIndex--;
                lastReturnedNode = nodeToReturn;

                if(lastReturnedNode != null) {
                    return lastReturnedNode.getElement();
                }
            }

            return null;
        }

        @Override
        public int nextIndex() {
            return nextIndex;
        }

        @Override
        public int previousIndex() {
            return nextIndex - 1;
        }

        @Override
        public void remove() throws NoSuchElementException {
            if (lastReturnedNode==null) throw new NoSuchElementException();
            checkForComodification();

            DoublyLinkedList.this.remove(lastReturnedNode);
            expectedModCount++;
            nextNode = prevNode.getNext();

            lastReturnedNode = null;
        }

        @Override
        public void set(E e) throws NoSuchElementException {
            if (lastReturnedNode==null) throw new NoSuchElementException();
            checkForComodification();
            
            lastReturnedNode.setElement(e);
        }

        @Override
        public void add(E e) {
            checkForComodification();

            DoublyLinkedList.this.addBetween(e, prevNode, prevNode.getNext());
            nextNode = prevNode.getNext();
            expectedModCount++;
            next();
        }

    }    //----------- end of inner DoublyLinkedListIterator class ----------
    
//---------------- Iterable implementation ----------------
    @Override
    public Iterator<E> iterator() {
        return new DoublyLinkedListIterator();
    }
    
    public ListIterator<E> listIterator() {
        return new DoublyLinkedListIterator();
    }
    
//---------------- nested Node class ----------------
    private static class Node<E> {

        private E element;      // reference to the element stored at this node
        private Node<E> prev;   // reference to the previous node in the list
        private Node<E> next;   // reference to the subsequent node in the list

        public Node(E element, Node<E> prev, Node<E> next) {
            this.element = element;
            this.prev = prev;
            this.next = next;
        }

        public E getElement() {
            return element;
        }

        public Node<E> getPrev() {
            return prev;
        }

        public Node<E> getNext() {
            return next;
        }
        
        public void setElement(E element) { // Not on the original interface. Added due to list iterator implementation
            this.element = element;
        }        

        public void setPrev(Node<E> prev) {
            this.prev = prev;
        }

        public void setNext(Node<E> next) {
            this.next = next;
        }
    } //----------- end of nested Node class ----------
    
}