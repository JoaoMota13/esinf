/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PL;

/**
 *
 * @author DEI-ESINF
 * @param <E>
 */
public class AVL <E extends Comparable<E>> extends BST<E> {

    private int balanceFactor(Node<E> node){
        if(node == null) {
            return 0;
        }

        int heightRight = 0;
        int heightLeft = 0;

        if(node.getRight() != null) {
            heightRight = super.height(node.getRight());
        }

        if(node.getLeft() != null) {
            heightLeft = super.height(node.getLeft());
        }

        return heightRight - heightLeft;
    }
    
    private Node<E> rightRotation(Node<E> node){
        if(node != null) {
            Node<E> nodeChild = node.getLeft();
            if(nodeChild == null) {
                return null;
            }

            node.setLeft(nodeChild.getRight());
            nodeChild.setRight(node);

            if(node.equals(root)) {
                root = nodeChild;
            }

            return node;
        }

        return null;
    }
    
    private Node<E> leftRotation(Node<E> node){
        if(node != null) {
            Node<E> nodeChild = node.getRight();
            if(nodeChild == null) {
                return null;
            }

            node.setRight(nodeChild.getLeft());
            nodeChild.setLeft(node);

            if(node.equals(root)) {
                root = nodeChild;
            }

            return node;
        }

        return null;
    }
    
    private Node<E> twoRotations(Node<E> node){
        if(node != null) {
            int bf = this.balanceFactor(node);

            if(bf < 0) {
                leftRotation(node.getLeft());
                return rightRotation(node);
                //return node;
            }
            else if(bf > 0)  {
                rightRotation(node.getRight());
                return leftRotation(node);
                //return node;
            }
        }

        return null;
    }
    
    private Node<E> balanceNode(Node<E> node)
    {
        int bf = this.balanceFactor(node);

        if(bf < -1 || bf > 1) {
            if(bf < -1) {
                int childNodeBf = this.balanceFactor(node.getLeft());
                if(childNodeBf < 0) {
                    return balanceNode(rightRotation(node));
                }
                else {
                    //return balanceNode(rightRotation(leftRotation(node)));
                    return balanceNode(twoRotations(node));
                    //return twoRotations(node);
                }
            } else if(bf > 1) {
                int childNodeBf = this.balanceFactor(node.getRight());
                if(childNodeBf > 0) {
                    return balanceNode(leftRotation(node));
                }
                else {
                    //return balanceNode(leftRotation(rightRotation(node)));
                    return balanceNode(twoRotations(node));
                    //return twoRotations(node);
                }
            }
        }

        return node;
    }
    
    @Override
    public void insert(E element){
        Node<E> node = insert(element, root);

        if (root == null) {
            root = node;
        }
    }

    private Node<E> insert(E element, Node<E> node){
        if(node == null) {
            return new Node<>(element, null, null);
        }

        if(node.getElement() == element) {
            node.setElement(element);
        }
        else {
            if(node.getElement().compareTo(element) > 0) {
                node.setLeft(insert(element, node.getLeft()));
            }
            else {
                node.setRight(insert(element, node.getRight()));
            }

            node = balanceNode(node);
        }

        return node;
    }
    
    @Override  
    public void remove(E element){
        root = remove(element, root());
    }

    private Node<E> remove(E element, BST.Node<E> node) {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    
    public boolean equals(Object otherObj) {

        if (this == otherObj) 
            return true;

        if (otherObj == null || this.getClass() != otherObj.getClass())
            return false;

        AVL<E> second = (AVL<E>) otherObj;
        return equals(root, second.root);
    }

    public boolean equals(Node<E> root1, Node<E> root2) {
        if (root1 == null && root2 == null) 
           return true;
        else if (root1 != null && root2 != null) {
            if (root1.getElement().compareTo(root2.getElement()) == 0) {
                return equals(root1.getLeft(), root2.getLeft())
                        && equals(root1.getRight(), root2.getRight());
            } else  
                return false; 
        }
        else return false;
    }
   
}