import java.lang.reflect.Array;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        System.out.println("a)");
        System.out.println(reverseString("João Mota"));
        System.out.println("");

        int m = 3;
        int n = 9;
        System.out.println("b)");
        System.out.println(String.format("%d * %d = %f", m, n, product(m, n)));
        System.out.println("");

        int a = 23732;
        int b = 180;
        System.out.println("c)");
        System.out.println(String.format("Greatest common divisor of %d and %d = %d", a, b, greatestCommonDivisor(a, b)));
        System.out.println("");

        System.out.println("d)");
        System.out.println(stringToInt("13531"));
        System.out.println("");

        System.out.println("e)");
        int[] arePalindromes = {99, 100, 101, 1221, 200, 300, 1313131};
        for (int number : arePalindromes) {
            System.out.println(String.format("%d is palindrome? %s", number, isPalindrome(number)));
        }
        System.out.println("");

        System.out.println("f)");
        int[][] matrix = {{1, 1, 1, 0, 1}, {1, 0, 1, 1, 1}, {1, 0, 0, 0, 1}, {1, 0, 0, 0, 1}, {1, 1, 1, 1, 1}};
        System.out.println(sumTwoDimensionArray(matrix));
        System.out.println("");

        System.out.println("Ex. 2");
        int[] labLine1 = {1, 1, 1, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1};
        int[] labLine2 = {1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1};
        int[] labLine3 = {1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1};
        int[] labLine4 = {1, 0, 0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1};
        int[] labLine5 = {1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0};
        int[] labLine6 = {0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0};
        int[] labLine7 = {0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1};

        int[][] fullLab = {labLine1, labLine2, labLine3, labLine4, labLine5, labLine6, labLine7};
        runLabirinth(fullLab);
    }

    // a)
    public static String reverseString(String value) {
        if (value != null && value.length() > 0) {
            return reverseString(value.substring(1)) + value.charAt(0);
        }

        return "";
    }

    //b
    public static float product(int m, int n) {
        if (n == 0) {
            return 0;
        }

        return m + product(m, n-1);
    }

    //c
    public static int greatestCommonDivisor(int a, int b) {
        if (b > a) {
            return greatestCommonDivisor(b, a);
        }

        if(b == 0) {
            return 0;
        }

        int restOfDivision = a % b;
        if (restOfDivision == 0) {
            return b;
        }
        else {
            return greatestCommonDivisor(b, restOfDivision);
        }
    }

    //d
    public static int stringToInt(String value) {
        if (value != null && value.length() > 0) {
            return (Integer.parseInt(value.substring(0, 1)) * (int)java.lang.Math.pow(10, value.length() - 1)) + stringToInt(value.substring(1));
        }

        return 0;
    }

    //e
    public static String isPalindrome(int number) {
        if (number != 0) {
            if (Integer.parseInt(reverseString(String.format("%d", number))) == number) {
                return "Yes";
            }
        }

        return "No";
    }

    //f
    public static int sumTwoDimensionArray(int[][] array) {
        if (array != null && array.length > 0 && array[0].length > 0) {
            return recursiveSumOfMatrix(array, 0, 0);
        }

        return 0;
    }

    public static int recursiveSumOfMatrix(int[][] matrix, int startAtLine, int startAtColumn) {
        if (startAtLine < matrix.length && startAtColumn < matrix[0].length) {
            return matrix[startAtLine][startAtColumn] + recursiveSumOfMatrix(matrix, startAtLine, startAtColumn +1);
        } else if(startAtLine < matrix.length) {
            return recursiveSumOfMatrix(matrix, startAtLine + 1, 0);
        }

        return 0;
    }

    public static void runLabirinth(int[][] labirinth) {
        //print initial matrix
        printMatrix(labirinth);

        move(labirinth, 0, 0, 6, 12);

        printMatrix(labirinth);
    }

    public static boolean move(int[][] labirinth, int y, int x, int targetY, int targetX) {
        if (y >= 0 && x >= 0 && labirinth != null && labirinth.length > y && labirinth[y].length > x) {
            if (labirinth[y][x] == 1) {
                labirinth[y][x] = 9;

                boolean result = move(labirinth, y - 1, x, targetY, targetX);
                if (!result) {
                    result = move(labirinth, y, x + 1, targetY, targetX);
                    if (!result) {
                        result = move(labirinth, y + 1, x, targetY, targetX);
                        if (!result) {
                            result = move(labirinth, y, x - 1, targetY, targetX);
                        }
                    }
                }

                if(!result) {
                    if(y == targetY && x == targetX) {
                        return true;
                    }

                    labirinth[y][x] = 2;
                }

                return result;
            }
        }

        return false;
    }

    public static boolean canMove(int[][] labirinth, int y, int x) {
        if (y >= 0 && x >= 0 && labirinth != null && labirinth.length > y && labirinth[y].length > x) {
            if (labirinth[y][x] == 1) {
                return true;
            }
        }

        return false;
    }

    public static void printMatrix(int[][] matrix) {
        if(matrix != null) {
            for (int i = 0; i < matrix.length; i++) {
                String line = "";

                for (int j = 0; j < matrix[i].length; j++) {
                    line = String.format("%s %d", line, matrix[i][j]);
                }

                System.out.println(line);
            }
            System.out.println("\n\n");
        }
    }

}
