package graph;

import graph.matrix.MatrixGraph;

import java.lang.reflect.Array;
import java.util.*;
import java.util.function.BinaryOperator;

/**
 *
 * @author DEI-ISEP
 *
 */
public class Algorithms {

    /** Performs breadth-first search of a Graph starting in a vertex
     *
     * @param g Graph instance
     * @param vert vertex that will be the source of the search
     * @return a LinkedList with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {
        if(!g.validVertex(vert)) {
            return null;
        }

        LinkedList<V> qbfs = new LinkedList<>();
        LinkedList<V> qaux = new LinkedList<>();
        ArrayList<V> visited = new ArrayList<>();

        qbfs.add(vert);
        qaux.add(vert);
        visited.add(vert);

        while(!qaux.isEmpty()) {
            vert = qaux.removeFirst();
            if(vert != null) {
                Collection<V> adjacentVerts = g.adjVertices(vert);

                if(adjacentVerts != null) {
                    for(V vAdj : adjacentVerts) {
                        if(!visited.contains(vAdj)) {
                            qbfs.add(vAdj);
                            qaux.add(vAdj);
                            visited.add(vAdj);
                        }
                    }
                }
            }
        }

        return qbfs;
    }

    /** Performs depth-first search starting in a vertex
     *
     * @param g Graph instance
     * @param vOrig vertex of graph g that will be the source of the search
     * @param visited set of previously visited vertices
     * @param qdfs return LinkedList with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs) {
        if(g.validVertex(vOrig)) {
            int vertexIndex = g.key(vOrig);

            if(!visited[vertexIndex]) {
                visited[vertexIndex] = true;
                qdfs.add(vOrig);

                Collection<V> adjacentVerts = g.adjVertices(vOrig);

                if(adjacentVerts != null) {
                    for(V adjV : adjacentVerts) {
                        DepthFirstSearch(g, adjV, visited, qdfs);
                    }
                }
            }
        }
    }

    /** Performs depth-first search starting in a vertex
     *
     * @param g Graph instance
     * @param vert vertex of graph g that will be the source of the search

     * @return a LinkedList with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {
        if(!g.validVertex(vert)) {
            return null;
        }

        boolean visited[] = new boolean[g.numVertices()];
        LinkedList<V> qdfs = new LinkedList<>();

        DepthFirstSearch(g, vert, visited, qdfs);

        return qdfs;
    }
    
    
    /** Returns all paths from vOrig to vDest
     *
     * @param g       Graph instance
     * @param vOrig   Vertex that will be the source of the path
     * @param vDest   Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path    stack with vertices of the current path (the path is in reverse order)
     * @param paths   ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest, boolean[] visited,
                                        LinkedList<V> path, ArrayList<LinkedList<V>> paths) {
        path.add(vOrig);
        int vertexKey = g.key(vOrig);
        for(V vertex : g.adjVertices(vOrig)) {
            if(vertex.equals(vDest)) {
                path.add(vDest);
                paths.add(path);
                path.removeFirst();
            } else if(!visited[vertexKey]) {
                allPaths(g, vOrig, vDest, visited, path, paths);
            }
        }
    }

    /** Returns all paths from vOrig to vDest
     *
     * @param g     Graph instance
     * @param vOrig information of the Vertex origin
     * @param vDest information of the Vertex destination
     * @return paths ArrayList with all paths from vOrig to vDest
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V vOrig, V vDest) {
        if(!g.validVertex(vOrig) || !g.validVertex(vDest)) {
            return null;
        }
        ArrayList<LinkedList<V>> returnList = new ArrayList<>();
        LinkedList<V> path = new LinkedList<>();
        boolean visited[] = new boolean[g.numVertices()];

        allPaths(g, vOrig, vDest, visited, path, returnList);

        return returnList;
    }
    
    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with non-negative edge weights
     * This implementation uses Dijkstra's algorithm
     *
     * @param g        Graph instance
     * @param vOrig    Vertex that will be the source of the path
     * @param visited  set of previously visited vertices
     * @param pathKeys minimum path vertices keys
     * @param dist     minimum distances
     */
    private static <V, E> void shortestPathDijkstraUnweighted(Graph<V, E> g, V vOrig,
                                                    Comparator<E> ce, BinaryOperator<E> sum, E zero,
                                                    boolean[] visited, V [] pathKeys, E [] dist) {
        LinkedList<V> qaux = new LinkedList<>();


        for(V vert : g.vertices()) {
            int vertexKey = g.key(vert);
            dist[vertexKey] = zero;
            pathKeys[vertexKey] = null;
            visited[vertexKey] = false;
        }
        qaux.add(vOrig);
        while(!qaux.isEmpty()) {
            vOrig = qaux.removeFirst();
            int vOrigKey = g.key(vOrig);
            visited[vOrigKey] = true;

            for(V vAdj : g.adjVertices(vOrig)) {
                int vAdjKey = g.key(vAdj);

                E edgeWeight = g.edge(vOrig, vAdj).getWeight();

                if(!visited[vAdjKey] && ce.compare(dist[vAdjKey], zero) == 0) {
                    pathKeys[vAdjKey] = vOrig;
                    dist[vAdjKey] = sum.apply(dist[vAdjKey], edgeWeight);
                    qaux.add(vAdj);
                }
            }
        }
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with non-negative edge weights
     * This implementation uses Dijkstra's algorithm
     *
     * @param g        Graph instance
     * @param vOrig    Vertex that will be the source of the path
     * @param visited  set of previously visited vertices
     * @param pathKeys minimum path vertices keys
     * @param dist     minimum distances
     */
    private static <V, E> void shortestPathDijkstra(Graph<V, E> g, V vOrig,
                                                    Comparator<E> ce, BinaryOperator<E> sum, E zero, E infinite,
                                                    boolean[] visited, V [] pathKeys, E [] dist) {
        for(V vert : g.vertices()) {
            int vertexKey = g.key(vert);
            dist[vertexKey] = infinite;
            pathKeys[vertexKey] = null;
            visited[vertexKey] = false;
        }
        int vOrigKey = g.key(vOrig);
        while(vOrigKey != -1) {
            visited[vOrigKey] = true;
            vOrig = g.vertex(vOrigKey);

            for(V vAdj : g.adjVertices(vOrig)) {
                int vAdjKey = g.key(vAdj);

                E edgeWeight = g.edge(vOrig, vAdj).getWeight();

                if(!visited[vAdjKey] && ce.compare(dist[vAdjKey], sum.apply(dist[vOrigKey], edgeWeight)) > 0) {
                    pathKeys[vAdjKey] = vOrig;
                    dist[vAdjKey] = sum.apply(dist[vOrigKey] == null ? zero : dist[vOrigKey], edgeWeight);
                    //visited[vAdjKey] = true;
                }
            }

            vOrigKey = getVertMinDist(dist, visited, ce, zero, infinite);
        }
    }

    private static <E> int getVertMinDist(E [] dist, boolean[] visited, Comparator<E> ce, E zero, E infinite) {
        E smallestEdgeWeight = infinite;
        int smallestIndex = -1;
        for (int i = 0; i < dist.length; i++) {
            if(!visited[i] && ce.compare(smallestEdgeWeight, dist[i]) > 0) {
                smallestEdgeWeight = dist[i];
                smallestIndex = i;
            }
        }

        return smallestIndex;
    }

   
    /** Shortest-path between two vertices
     *
     * @param g graph
     * @param vOrig origin vertex
     * @param vDest destination vertex
     * @param ce comparator between elements of type E
     * @param sum sum two elements of type E
     * @param zero neutral element of the sum in elements of type E
     * @param shortPath returns the vertices which make the shortest path
     * @return if vertices exist in the graph and are connected, true, false otherwise
     */
    public static <V, E> E shortestPath(Graph<V, E> g, V vOrig, V vDest,
                                        Comparator<E> ce, BinaryOperator<E> sum, E zero, E infinite,
                                        LinkedList<V> shortPath) {
        Stack<V> stack = new Stack<>();
        boolean foundPath = false;

        shortPath.clear();

        if(!g.validVertex(vOrig) || !g.validVertex(vDest)) {
            return null;
        }

        E totalDist = zero;

        if(vOrig.equals(vDest)) {
            stack.add(vDest);
            foundPath = true;
        }
        else {
            @SuppressWarnings("unchecked")
            V [] pathKeys = (V[])new Object[g.vertices().size()];

            @SuppressWarnings("unchecked")
            E [] dist = (E[])new Object[g.vertices().size()];

            boolean [] visited = new boolean[g.vertices().size()];

            shortestPathDijkstra(g, vOrig, ce, sum, zero, infinite, visited, pathKeys, dist);

            int vDestKey = g.key(vDest);

            V precedent = pathKeys[vDestKey];

            if(precedent != null) {
                totalDist = sum.apply(totalDist, dist[vDestKey]);
                stack.add(vDest);

                while(precedent != null) {
                    if(vOrig.equals(precedent)) {
                        foundPath = true;
                    }

                    int precedentKey = g.key(precedent);
                    stack.add(precedent);

                    totalDist = sum.apply(totalDist, dist[precedentKey]);
                    precedent = pathKeys[precedentKey];
                }
            }
        }

        if(foundPath && !stack.isEmpty()) {
            while(!stack.isEmpty()) {
                shortPath.add(stack.pop());
            }

            if(vOrig.equals(vDest)) {
                return zero;
            }

            return totalDist;
        }

        return null;
    }

    /** Shortest-path between a vertex and all other vertices
     *
     * @param g graph
     * @param vOrig start vertex
     * @param ce comparator between elements of type E
     * @param sum sum two elements of type E
     * @param zero neutral element of the sum in elements of type E
     * @param paths returns all the minimum paths
     * @param dists returns the corresponding minimum distances
     * @return if vOrig exists in the graph true, false otherwise
     */
    public static <V, E> boolean shortestPaths(Graph<V, E> g, V vOrig,
                                               Comparator<E> ce, BinaryOperator<E> sum, E zero,
                                               ArrayList<LinkedList<V>> paths, ArrayList<E> dists) {

        throw new UnsupportedOperationException("Not supported yet.");
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf
     * The path is constructed from the end to the beginning
     *
     * @param g        Graph instance
     * @param vOrig    information of the Vertex origin
     * @param vDest    information of the Vertex destination
     * @param pathKeys minimum path vertices keys
     * @param path     stack with the minimum path (correct order)
     */
    private static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest,
                                       V [] pathKeys, LinkedList<V> path) {

        throw new UnsupportedOperationException("Not supported yet.");
    }

    /** Calculates the minimum distance graph using Floyd-Warshall
     * 
     * @param g initial graph
     * @param ce comparator between elements of type E
     * @param sum sum two elements of type E
     * @return the minimum distance graph
     */
    public static <V,E> MatrixGraph <V,E> minDistGraph(Graph <V,E> g, Comparator<E> ce, BinaryOperator<E> sum) {
        
        throw new UnsupportedOperationException("Not supported yet.");
    }

}