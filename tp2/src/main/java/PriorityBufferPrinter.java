import java.util.ArrayList;
import java.util.Iterator;

public class PriorityBufferPrinter<T extends Document> implements Iterable<T> {

    private final ArrayList<T> buffer;
    private final Integer maxSize;

    public PriorityBufferPrinter(Integer maximumSize) {
        maxSize = maximumSize;
        buffer = new ArrayList<T>();
    }

    public boolean addDocument(T document) {
        int currentOccupiedMemory = 0;

        for(T doc : buffer) {
            currentOccupiedMemory += document.getSize();
        }

        if (maxSize >= currentOccupiedMemory + document.getSize()) {
            for (T doc : buffer) {
                if(doc.getPriority() < document.getPriority()) {
                    buffer.add(buffer.indexOf(doc), document);
                    return true;
                }
            }

            buffer.add(document);
            return true;
        }

        return false;
    }

    public T getDocument() {
        if (buffer.size() > 0) {
            T auxReturn = buffer.get(0);
            buffer.remove(0);

            return auxReturn;
        }
        return null;
    }

    public boolean delDocument(String name, String author) {
        int indexToRemove = -1;

        for (T doc : buffer) {
            if (name.equalsIgnoreCase((doc.getName())) && author.equalsIgnoreCase(doc.getAuthor())) {
                indexToRemove = buffer.indexOf(doc);
                break;
            }
        }

        if(indexToRemove != -1) {
            buffer.remove(indexToRemove);
            return true;
        }

        return false;
    }

    public boolean delDocumentsAbove(int maxDocumentSize) {
        ArrayList<T> docsToRemove = new ArrayList<T>();

        for(T doc : buffer) {
            if (doc.getSize() > maxDocumentSize) {
                docsToRemove.add(doc);
            }
        }

        if (docsToRemove.size() > 0) {
            buffer.removeAll(docsToRemove);
            return true;
        }

        return false;
    }

    public Iterator<T> iterator() {
        return null;
    }
}
