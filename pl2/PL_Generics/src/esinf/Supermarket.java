/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package esinf;

import java.security.KeyPair;
import java.time.LocalDate;
import java.util.*;

/**
 *
 * @author DEI-ISEP
 */
public class Supermarket {
    Map <Invoice, Set<Product>> sup;
    
    Supermarket() {
        sup = new HashMap<>();
    }
    
    // Reads invoices from a list of String
    void getInvoices(List <String> l) throws Exception {
        Invoice currentInvoice = null;
        Set<Product> currentProducts = new HashSet<>();

        for(String line : l) {
            String[] splitRow = line.split(",");

            if (splitRow != null && splitRow.length > 0) {
                if(splitRow[0].equalsIgnoreCase("I")) {

                    //If any invoice is created, finish previous iteration by adding invoice and products to the Map
                    //and only after creating a new Invoice instance.
                    if (currentInvoice != null && currentProducts != null) {
                        sup.put(currentInvoice, currentProducts);
                        currentProducts = new HashSet<>();
                    }

                    currentInvoice = new Invoice(splitRow[1], splitRow[2]);

                } else if(splitRow[0].equalsIgnoreCase("P")) {
                    currentProducts.add(new Product(splitRow[1], Integer.parseInt(splitRow[2]), Long.parseLong(splitRow[3])));
                }
            }
        }

        //Final iteration is added here because it does not fall inside another Invoice line inside the loop.
        if(!sup.containsKey(currentInvoice)) {
            sup.put(currentInvoice, currentProducts);
        }
    }   
    
    // returns a set in which each number is the number of products in the r
    // invoice 
    Map<Invoice, Integer> numberOfProductsPerInvoice() {
        Map<Invoice, Integer> returnMap = new HashMap<>();

        for(Map.Entry<Invoice, Set<Product>> entry : sup.entrySet()) {
            if(!returnMap.containsKey(entry.getKey())) {
                returnMap.put(entry.getKey(), entry.getValue().size());
            }
        }

        return returnMap;
    }

    // returns a Set of invoices in which each date is >d1 and <d2
    Set<Invoice> betweenDates(LocalDate d1, LocalDate d2) {
        Set<Invoice> invoicesToReturn = new HashSet<>();

        for(Map.Entry<Invoice, Set<Product>> entry : sup.entrySet()) {
            if (entry.getKey().getDate().compareTo(d1) > 0 && entry.getKey().getDate().compareTo(d2) < 0) {
                invoicesToReturn.add(entry.getKey());
            }
        }

        return invoicesToReturn;
    }
    
    // returns the sum of the price of the product in all the invoices
    long totalOfProduct(String productId) {
        long sumOfPrices = 0;

        for(Map.Entry<Invoice, Set<Product>> entry : sup.entrySet()) {
            for (Product prod : entry.getValue()) {
                if (prod.getIdentification().equalsIgnoreCase(productId)) {
                    sumOfPrices += prod.getPrice() * prod.getQuantity();
                }
            }
        }

        return sumOfPrices;
    }
    
    // converts a map of invoices and products to a map which key is a product
    // identification and the values are a set of the invoices in which it appears
    Map<String, Set<Invoice>> convertInvoices() {
        Map<String, Set<Invoice>> returnMap = new HashMap<>();

        for (Map.Entry<Invoice, Set<Product>> entry : sup.entrySet()) {
            for (Product prod : entry.getValue()) {
                if (!returnMap.containsKey(prod.getIdentification())) {
                    returnMap.put(prod.getIdentification(), new HashSet<>());
                }

                if (!returnMap.get(prod.getIdentification()).contains(entry.getKey())) {
                    returnMap.get(prod.getIdentification()).add(entry.getKey());
                }
            }
        }

        return returnMap;
    }
}
